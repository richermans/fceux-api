#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import (print_function, unicode_literals,
                        absolute_import, division)

from emulator import MarioEmulator

class Mario(MarioEmulator):
    def gameStarted(self):
        print("game started!")

    def frame(self, n, screenshot):
        if n % 60 == 0: # jump every second
            return self.BUTTON_A
        return self.BUTTON_RIGHT

    def marioDied(self, n):
        print("died in frame", n, ":(")

    def levelFinished(self, n):
        print("level finished in frame", n, ":)")

mario = Mario()
mario.run()
